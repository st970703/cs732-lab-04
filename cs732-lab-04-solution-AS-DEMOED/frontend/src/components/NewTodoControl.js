import { useState, useContext } from 'react';
import { AppContext } from '../AppContextProvider';
import dayjs from 'dayjs';

export default function NewTodoControl() {
    const { createTodo } = useContext(AppContext);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [dueDate, setDueDate] = useState(dayjs().add(1, 'week'));

    function handleAdd() {
        createTodo({
            title,
            description,
            dueDate,
            isComplete: false
        });
    }

    return (
        <>
            <tr>
                <td><input aria-label="Title" id="new-todo-title" type="text" value={title} onInput={e => setTitle(e.target.value)} /></td>
                <td><input aria-label="Due date" id="new-todo-duedate" type="date" value={dueDate.format('YYYY-MM-DD')} onChange={e => setDueDate(dayjs(e.target.value))} /></td>
            </tr>
            <tr>
                <td colSpan={3}>
                    <input aria-label="Description" id="new-todo-desc" type="text" value={description} onInput={e => setDescription(e.target.value)} style={{ width: '100%' }} />
                </td>
                <td>
                    <button onClick={handleAdd}>Add</button>
                </td>
            </tr>
        </>
    )
}