import dayjs from 'dayjs';

function getStatus(todo) {
    const { isComplete, dueDate } = todo;

    if (isComplete) {
        return 'Complete';
    }
    else if (dayjs().isAfter(dueDate)) {
        return 'Overdue';
    }
    else if (dayjs().add(3, 'day').isAfter(dueDate)) {
        return 'Upcoming';
    }
    else {
        return 'Incomplete';
    }
}

export default function TodoView({ todo, onToggleComplete, onDelete }) {

    return (
        <>
            <tr>
                <td>{todo.title}</td>
                <td>{dayjs(todo.dueDate).fromNow()}</td>
                <td>
                    <label>
                        <input type="checkbox" checked={todo.isComplete} onChange={e => onToggleComplete(todo, e.target.checked)} />
                        {getStatus(todo)}
                    </label>
                </td>
                <td><button onClick={e => onDelete(todo)}>Delete</button></td>
            </tr>
            <tr className="description">
                <td colSpan={4}>
                    {todo.description}
                </td>
            </tr>
        </>
    );
}