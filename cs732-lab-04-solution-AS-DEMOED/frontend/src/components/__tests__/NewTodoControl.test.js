import NewTodoControl from '../NewTodoControl';
import dayjs from 'dayjs';
import { render, fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect';
import { AppContext } from '../../AppContextProvider';
import { DuoOutlined } from '@material-ui/icons';

const relativeTime = require('dayjs/plugin/relativeTime');
dayjs.extend(relativeTime);

it('Renders, saves state, and fires events', () => {

    const dummyCreateTodo = jest.fn();

    render(
        <AppContext.Provider value={{ createTodo: dummyCreateTodo }}>
            <NewTodoControl />
        </AppContext.Provider>
    );

    const txtTitle = screen.getByLabelText('Title');
    userEvent.type(txtTitle, 'Test title');

    const txtDescription = screen.getByLabelText('Description');
    userEvent.type(txtDescription, 'Test description');

    const txtDate = screen.getByLabelText('Due date');
    userEvent.type(txtDate, '2100-01-01');

    const addButton = screen.getByText('Add');
    fireEvent.click(addButton);

    expect(dummyCreateTodo).toHaveBeenCalledTimes(1);
    const todo = dummyCreateTodo.mock.calls[0][0];

    expect(todo.title).toBe('Test title');
    expect(todo.description).toBe('Test description');
    expect(todo.isComplete).toBe(false);
    expect(dayjs(todo.dueDate)).toEqual(dayjs('2100-01-01'));

});