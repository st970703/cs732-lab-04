import TodoView from '../TodoView';
import dayjs from 'dayjs';
import { render, fireEvent, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

const relativeTime = require('dayjs/plugin/relativeTime');
dayjs.extend(relativeTime);

const completedTodo = {
    _id: '605cf065e2f8ff1070e22503',
    title: 'CompleteTodo',
    description: 'CompleteDesc',
    isComplete: true,
    dueDate: dayjs()
}

const overdueTodo = {
    _id: '605cf065e2f8ff1070e22503',
    title: 'OverdueTodo',
    description: 'OverdueDesc',
    isComplete: false,
    dueDate: dayjs().subtract(1, 'day')
}

const upcomingTodo = {
    _id: '605cf065e2f8ff1070e22503',
    title: 'UpcomingTodo',
    description: 'UpcomingDesc',
    isComplete: false,
    dueDate: dayjs().add(1, 'day')
}

const futureTodo = {
    _id: '605cf065e2f8ff1070e22503',
    title: 'FutureTodo',
    description: 'FutureDesc',
    isComplete: false,
    dueDate: dayjs().add(1, 'week')
}

it('Renders a completed todo item', () => {

    const onToggle = jest.fn();
    const onDelete = jest.fn();

    render(<TodoView todo={completedTodo} onToggleComplete={onToggle} onDelete={onDelete} />);

    expect(screen.queryByText('CompleteTodo')).not.toBeNull();
    expect(screen.queryByText('CompleteDesc')).not.toBeNull();

    const checkbox = screen.getByText('Complete');
    fireEvent.click(checkbox);
    expect(onToggle).toHaveBeenCalledTimes(1);

    const deleteButton = screen.getByText('Delete');
    fireEvent.click(deleteButton);
    expect(onDelete).toHaveBeenCalledTimes(1);

})

it('Renders an upcoming todo item', () => {

    render(<TodoView todo={upcomingTodo} />);

    expect(screen.queryByText('UpcomingTodo')).not.toBeNull();
    expect(screen.queryByText('UpcomingDesc')).not.toBeNull();
    expect(screen.queryByText('Upcoming')).not.toBeNull();

})

it('Renders a future todo item', () => {

    render(<TodoView todo={futureTodo} />);

    expect(screen.queryByText('FutureTodo')).not.toBeNull();
    expect(screen.queryByText('FutureDesc')).not.toBeNull();
    expect(screen.queryByText('Incomplete')).not.toBeNull();

})

it('Renders an overdue todo item', () => {

    render(<TodoView todo={overdueTodo} />);

    expect(screen.queryByText('OverdueTodo')).not.toBeNull();
    expect(screen.queryByText('OverdueDesc')).not.toBeNull();
    expect(screen.queryByText('Overdue')).not.toBeNull();

})