import { useContext } from 'react';
import { AppContext } from '../AppContextProvider';
import NewTodoControl from './NewTodoControl';
import TodoView from './TodoView';

export default function TodoList() {

    const { todos, todosLoading, updateTodo, deleteTodo } = useContext(AppContext);

    function handleToggleComplete(todo, isComplete) {
        updateTodo({ ...todo, isComplete });
    }

    function handleDelete(todo) {
        deleteTodo(todo._id);
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Due date</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                <NewTodoControl />
            </thead>
            <tbody>
                {todosLoading ? (
                    <tr>
                        <td colSpan={4}>Loading...</td>
                    </tr>
                ) : todos.map(todo => (
                    <TodoView key={todo._id} todo={todo} onDelete={handleDelete} onToggleComplete={handleToggleComplete} />
                ))}
            </tbody>
        </table>
    );
}