module.exports = {
    testEnvironment: 'node',
    roots: [
        './src'
    ],
    testTimeout: 30000
}