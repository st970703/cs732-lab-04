import dayjs from 'dayjs';
import mongoose from 'mongoose';

// https://stackoverflow.com/questions/54477404/disabling-the-your-test-suite-must-contain-at-least-one-test-rule-in-jest
test.skip('Workaround', () => 1);

const todo1 = {
    _id: new mongoose.mongo.ObjectId("000000000000000000000002"),
    title: "dolor sit amet consectetur adipiscing",
    dueDate: dayjs().add(1, 'day').format(),
    isCompleted : true,
    description: "purus sit amet luctus venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non enim praesent elementum facilisis leo vel"
};

const todo2 = {
    _id: new mongoose.mongo.ObjectId("000000000000000000000003"),
    title: "est velit egestas dui id",
    dueDate: dayjs().subtract(1, 'day').format(),
    isCompleted : false,
    description: ""
};

const todo3 = {
    _id: new mongoose.mongo.ObjectId("000000000000000000000004"),
    title: "ornare quam viverra",
    dueDate: dayjs().add(2, 'day').format(),
    isCompleted : false,
    description: "feugiat nisl pretium fusce id velit ut tortor pretium viverra suspendisse potenti nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi lacus sed viverra tellus in hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at augue eget arcu dictum varius duis at consectetur lorem donec massa sapien faucibus et molestie ac feugiat sed lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare massa eget egestas purus viverra accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu augue ut lectus arcu bibendum at varius vel pharetra vel turpis nunc eget"
};

const todo4 = {
    _id: new mongoose.mongo.ObjectId("000000000000000000000005"),
    title: "ut enim blandit volutpat maecenas volutpat",
    dueDate: dayjs().subtract(2, 'day').format(),
    isCompleted : true,
    description: "scelerisque eu ultrices vitae auctor eu augue ut lectus arcu bibendum at varius vel pharetra vel turpis nunc eget lorem dolor sed viverra ipsum nunc aliquet bibendum enim facilisis gravida neque convallis a cras semper auctor neque vitae tempus quam pellentesque nec nam aliquam sem et tortor consequat id porta nibh venenatis cras sed felis eget velit aliquet sagittis id consectetur purus ut faucibus pulvinar elementum integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam ut venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo vel orci porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam"
};

const newTodo = {
    _id: new mongoose.mongo.ObjectId("000000000000000000000006"),
    title: "purus faucibus ornare suspendisse",
    dueDate: dayjs().add(3, 'day').format(),
    isCompleted : false,
    description: "amet facilisis magna etiam tempor orci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui nunc mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa id neque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id ornare arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum est ultricies integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc consequat interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut consequat semper"
};

const todoNoTitle = {
    title: "",
    dueDate: dayjs().subtract(3, 'day').format(),
    isCompleted : false,
    description: "amet facilisis magna etiam tempor orci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui nunc mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa id neque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id ornare arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum est ultricies integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc consequat interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut consequat semper"
};

const todo2Modified = {
    title: "new Todo title",
    dueDate: dayjs().format(),
    isCompleted: !todo2.isCompleted,
    description: "new Todo Description"
};

const nonexistantId = "9999999999999999999999909";

export {todo1, todo2, todo3, todo4, newTodo, todoNoTitle, todo2Modified, nonexistantId}