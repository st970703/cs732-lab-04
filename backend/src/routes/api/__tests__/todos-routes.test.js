import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { todo1, todo2, todo3, todo4, newTodo, todoNoTitle, todo2Modified, nonexistantId } from './data/test-dummy-data';
import express from 'express';
import axios from 'axios';
import router from '../todos-routes';
import connectToDatabase from '../../../db/db-connect';
import { Todo } from '../../../db/todos-schema';
import dayjs from 'dayjs';


let mongod, app, server;

const HTTP_OK = 200;
const HTTP_CREATED = 201;
const HTTP_NOT_FOUND = 404;
const HTTP_BAD_REQUEST = 400;
const HTTP_NO_CONTENT = 204;

const PORT_NO = 3002;


beforeAll(async done => {
    mongod = new MongoMemoryServer();

    const connectionString = await mongod.getUri();
    connectToDatabase(connectionString);

    app = express();

    // note: must parse to json before processing
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));

    app.use('/api/todos', router);

    server = app.listen(PORT_NO, () => done());
});

beforeEach(async () => {
    const dummyTodos = [todo1, todo2, todo3, todo4];

    await Todo.insertMany(dummyTodos);
});

afterEach(async () => {
    await Todo.deleteMany({});
});

afterAll(done => {
    server.close(async () => {
        await mongoose.disconnect();
        await mongod.stop();
        done();
    });
});


it('Retrieves all todos', async () => {
    const response = await axios.get('http://localhost:' + PORT_NO + '/api/todos');
    const todos = response.data;

    expect(response.status).toBe(HTTP_OK);
    expect(todos).toBeTruthy();
    expect(todos.length).toBe(4);

    expect(todos[0].title).toBe(todo1.title);
    expect(dayjs(todos[0].dueDate)).toStrictEqual(dayjs(todo1.dueDate));
    expect(todos[0].description).toBe(todo1.description);
    expect(todos[0].isCompleted).toBe(todo1.isCompleted);

    expect(todos[1].title).toBe(todo2.title);
    expect(dayjs(todos[1].dueDate)).toStrictEqual(dayjs(todo2.dueDate));
    expect(todos[1].description).toBe(todo2.description);
    expect(todos[1].isCompleted).toBe(todo2.isCompleted);

    expect(todos[2].title).toBe(todo3.title);
    expect(dayjs(todos[2].dueDate)).toStrictEqual(dayjs(todo3.dueDate));
    expect(todos[2].description).toBe(todo3.description);
    expect(todos[2].isCompleted).toBe(todo3.isCompleted);

    expect(todos[3].title).toBe(todo4.title);
    expect(dayjs(todos[3].dueDate)).toStrictEqual(dayjs(todo4.dueDate));
    expect(todos[3].description).toBe(todo4.description);
    expect(todos[3].isCompleted).toBe(todo4.isCompleted);
});

it('Retrieves a single todo', async () => {
    const response = await axios.get('http://localhost:' + PORT_NO + '/api/todos/' + todo2._id);
    expect(response.status).toBe(HTTP_OK);

    const todo = response.data;
    expect(todo.title).toBe(todo2.title);
    expect(dayjs(todo.dueDate)).toStrictEqual(dayjs(todo2.dueDate));
    expect(todo.description).toBe(todo2.description);
    expect(todo.isCompleted).toBe(todo2.isCompleted);
});

it('Responds with a 404 when trying to retrieve a nonexistant todo', async () => {
    try {
        const response = await axios.get('http://localhost:' + PORT_NO + '/api/todos/' + nonexistantId);
        fail('Should fail with a 404 when trying to retrieve a nonexistant todo');
    } catch (err) {
        const { response } = err;
        expect(response).toBeDefined();
        expect(response.status).toBe(HTTP_BAD_REQUEST);
        expect(response.data).toBe('Invalid ID');
    }
});

it('Creates and adds a new todo', async () => {
    const postResponse = await axios.post('http://localhost:' + PORT_NO + '/api/todos', newTodo)
        .then(response => {
            expect(response.status).toBe(HTTP_CREATED);

            const todo = response.data;
            expect(todo.title).toBe(newTodo.title);
            expect(dayjs(todo.dueDate)).toStrictEqual(dayjs(newTodo.dueDate));
            expect(todo.description).toBe(newTodo.description);
            expect(todo.isCompleted).toBe(newTodo.isCompleted);
            
            expect(response.headers.location).toBe(`/api/todos/${newTodo._id}`);
        });

    let getResponse = await axios.get('http://localhost:' + PORT_NO + '/api/todos');
    const todos = getResponse.data;

    expect(todos).toBeTruthy();
    expect(todos.length).toBe(5);

    getResponse = await axios.get('http://localhost:' + PORT_NO + '/api/todos/' + newTodo._id);
    const todo = getResponse.data;
    expect(todo.title).toBe(newTodo.title);
    expect(dayjs(todo.dueDate)).toStrictEqual(dayjs(newTodo.dueDate));
    expect(todo.description).toBe(newTodo.description);
    expect(todo.isCompleted).toBe(newTodo.isCompleted);
});

it('Responds with a 400 status when trying to create a new todo without a title', async () => {
    try {
        const response = await axios.post('http://localhost:' + PORT_NO + '/api/todos', todoNoTitle);
        fail('Should fail with a 404 when trying to create a todo without title');
    } catch (err) {
        const { response } = err;
        expect(response).toBeDefined();
        expect(response.status).toBe(HTTP_BAD_REQUEST);
        expect(response.data).toBe("New todos must have a title");
    } finally {
        const response = await axios.get('http://localhost:' + PORT_NO + '/api/todos');
        const todos = response.data;

        expect(todos).toBeTruthy();
        expect(todos.length).toBe(4);
    }
});

it('Updates an existing todo', async () => {
    const putResponse = await axios.put('http://localhost:' + PORT_NO + '/api/todos/' + todo2._id, todo2Modified);
    expect(putResponse.status).toBe(HTTP_NO_CONTENT);

    let getResponse = await axios.get('http://localhost:' + PORT_NO + '/api/todos');
    const todos = getResponse.data;

    expect(todos).toBeTruthy();
    expect(todos.length).toBe(4);

    getResponse = await axios.get('http://localhost:' + PORT_NO + '/api/todos/' + todo2._id);
    const todo = getResponse.data;

    expect(todo.title).toBe(todo2Modified.title);
    expect(dayjs(todo.dueDate)).toStrictEqual(dayjs(todo2Modified.dueDate));
    expect(todo.description).toBe(todo2Modified.description);
    expect(todo.isCompleted).toBe(todo2Modified.isCompleted);
});


it('Responds with a 404 when trying to update a nonexistant todo', async () => {
    try {
        const putResponse = await axios.put('http://localhost:' + PORT_NO + '/api/todos/' + nonexistantId, todo2Modified);
        fail('Should fail with a 404 when trying to update a nonexistant todo');
    } catch (err) {
        const { response } = err;
        expect(response).toBeDefined();
        expect(response.status).toBe(HTTP_BAD_REQUEST);
        expect(response.data).toBe('Invalid ID');
    } finally {
        let getResponse = await axios.get('http://localhost:' + PORT_NO + '/api/todos');
        const todos = getResponse.data;

        expect(todos).toBeTruthy();
        expect(todos.length).toBe(4);
    }
});

it('Deletes a todo successuflly, and response with the correct status code', async () => {
    const deleteResponse = await axios.delete('http://localhost:' + PORT_NO + '/api/todos/' + todo3._id);

    expect(deleteResponse.status).toBe(HTTP_NO_CONTENT);
    expect(deleteResponse.statusText).toBe('No Content');

    const getResponse = await axios.get('http://localhost:' + PORT_NO + '/api/todos');
    const todos = getResponse.data;

    expect(todos).toBeTruthy();
    expect(todos.length).toBe(3);

    try {
        const response = await axios.get('http://localhost:' + PORT_NO + '/api/todos/' + todo3._id);
        fail('Should fail with a 404 when trying to retrieve a deleted todo');
    } catch (err) {
        const { response } = err;
        expect(response).toBeDefined();
        expect(response.status).toBe(HTTP_NOT_FOUND);
    }
});

it('When trying to delete a nonexistant todo, gives the correct response status', async () => {
    try {
        const deleteResponse = await axios.delete('http://localhost:' + PORT_NO + '/api/todos/' + nonexistantId);
        fail('Deleting a nonexistant todo Should fail');
    } catch (err) {
        const { response } = err;
        expect(response).toBeDefined();
        expect(response.status).toBe(HTTP_BAD_REQUEST);
    } finally {
        const getResponse = await axios.get('http://localhost:' + PORT_NO + '/api/todos');
        const todos = getResponse.data;

        expect(todos).toBeTruthy();
        expect(todos.length).toBe(4);
    }
});