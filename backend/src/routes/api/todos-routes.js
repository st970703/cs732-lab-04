import express from 'express';
import mongoose from 'mongoose';
import {
    createTodo,
    retrieveAllTodos,
    retrieveTodo,
    updateTodo,
    deleteTodo
} from '../../db/todos-dao';

// const HTTP_OK = 200; // Not really needed; this is the default if you don't set something else.
const HTTP_CREATED = 201;
const HTTP_NOT_FOUND = 404;
const HTTP_BAD_REQUEST = 400;
const HTTP_NO_CONTENT = 204;

const router = express.Router();

// TODO Exercise Four: Add your RESTful routes here.
router.use("/:id", async (req, res, next) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(HTTP_BAD_REQUEST)
            .contentType('text/plain')
            .send('Invalid ID');
    } else {
        // important!!! This lets other router methods handle corresponding requests.    
        next();
    }
});

router.post("/", async (req, res) => {
    if (!req.body.title) {
        res.status(HTTP_BAD_REQUEST)
        .contentType('text/plain')
        .send('New todos must have a title');
        return;
    }

    const newTodo = await createTodo(req.body);

    res.status(HTTP_CREATED)
        .header('Location', `/api/todos/${newTodo._id}`)
        .json(newTodo);
});


router.get("/", async (req, res) => {
    res.json(await retrieveAllTodos());
});

router.get("/:id", async (req, res) => {
    const { id } = req.params;

    const todo = await retrieveTodo(id);

    if (todo) {
        res.json(todo);
    } else {
        res.status(HTTP_NOT_FOUND)
        .contentType('text/plain')
        .send('No todos found');
    }
});

router.put("/:id", async (req, res) => {
    if (!req.body.title) {
        res.status(HTTP_BAD_REQUEST)
        .contentType('text/plain')
        .send('New todos must have a title');
        return;
    }

    const { id } = req.params;

    let newTodo = { ...req.body };

    newTodo._id = req.params.id;

    const success = await updateTodo(newTodo);

    res.sendStatus(success ? HTTP_NO_CONTENT : HTTP_NOT_FOUND);
});

router.delete("/:id", async (req, res) => {

    const { id } = req.params;

    const result = await deleteTodo(id);

    if (result) {
        res.sendStatus(HTTP_NO_CONTENT);
    } else {
        res.sendStatus(HTTP_BAD_REQUEST);
    }
});

export default router;