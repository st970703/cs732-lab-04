import mongoose from 'mongoose';
import dayjs from 'dayjs';

const Schema = mongoose.Schema;


// TODO Exercise One: Model your schema here. Make sure to export it!
const todoSchema = new Schema({
    title: { type: String, required: true },
    dueDate: { type: Date, required: true },
    isCompleted: { type: Boolean, required: true },
    description: String,
}, {
    timestamps: {}
});

function countWords(str) {
    return str.trim().split(/\s+/).length;
  }

todoSchema.virtual('descriptionLength')
.get(
    function() {
        return countWords(this.description);
    }
);

todoSchema.virtual('isIncompleteButNotOverdue')
.get(
    function() {
        const today = dayjs();
        const dueDate = dayjs(this.dueDate);
        const result = !this.isCompleted && dueDate.isAfter(today);

        return result;
    }
);

todoSchema.virtual('isOverdue')
.get(
    function() {
        const today = dayjs();
        const dueDate = dayjs(this.dueDate);
        const result = !this.isCompleted && today.isAfter(dueDate);

        return result;
    }
);

const Todo = mongoose.model('Todo', todoSchema);

export {Todo};