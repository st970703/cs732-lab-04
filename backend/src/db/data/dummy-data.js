import dummyjson from 'dummy-json';
import fs from 'fs';
import dayjs from 'dayjs';

const myHelpers = {
    daysFromNow(min, max) {
        const minTime = dayjs().add(min, 'day').toDate().getTime();
        const maxTime = dayjs().add(max, 'day').toDate().getTime();
        const newTime = dummyjson.utils.randomInt(minTime, maxTime);
        return dayjs(new Date(newTime)).format();
    }
}

const template = fs.readFileSync('./src/db/data/dummy-data.hbs', { encoding: 'utf-8' });
const todosJson = dummyjson.parse(template, { helpers: myHelpers });
const dummyTodos = JSON.parse(todosJson);

export {
    dummyTodos
};