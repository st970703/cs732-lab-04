/**
 * This program should be run in order to populate the database with dummy data for testing purposes.
 */

import mongoose from 'mongoose';
import connectToDatabase from './db-connect';
import { Todo } from './todos-schema';
import { dummyTodos } from './data/dummy-data';

main();

async function main() {
    await connectToDatabase();
    console.log('Connected to database!');
    console.log();

    await clearDatabase();
    console.log();

    await addData();
    console.log();

    await logSummary();
    console.log();

    await runQueries();
    console.log();

    // Disconnect when complete
    await mongoose.disconnect();
    console.log('Disconnected from database!');
}

// TODO Exercise Two: Complete the clearDatabase() and addData() functions below.

async function clearDatabase() {
    const todosDeleted = await Todo.deleteMany({});
    console.log(`Cleared database (removed ${todosDeleted.deletedCount} todos).`);
}

async function addData() {
    const result = await Todo.insertMany(
    dummyTodos.todos.map(todo => new Todo(todo)));
    console.log('Added ' + result.length + ' todos to the databse.');

    for (let todo of result) {
        console.log(' - ' + todo.title + ' ' + todo._id);
    }
}

async function logSummary() {
    const allTodos = await Todo.find();

    console.log(`There are ${allTodos.length} todos in the database.`);
    console.log();
}



async function runQueries() {

// A todo with a short description (20 words or less)
const todos = await Todo.find();
const todosShortDesc = todos.filter(todo => todo.descriptionLength <= 20);
console.log(`There are ${todosShortDesc.length} todos with short descriptions in the database.`);

// A todo with a long description (100 words or more)
const todosLongDesc = todos.filter(todo => todo.descriptionLength >= 100);
console.log(`There are ${todosLongDesc.length} todos with long descriptions in the database.`);

// A todo with no description
const todosNoDesc = await Todo.find({ description: "" });
console.log(`There are ${todosNoDesc.length} todos with no descriptions in the database.`);

// A completed item
const completedTodos = await Todo.find({ isCompleted: true });
console.log(`There are ${completedTodos.length} completed todos in the database.`);

// An incomplete but not overdue item
const todosIncompleteNotOverdue = todos.filter(todo => todo.isIncompleteButNotOverdue);
console.log(`There are ${todosIncompleteNotOverdue.length} incomplete but not overdue todos in the database.`);

// An overdue item
const todosOverdue = todos.filter(todo => todo.isOverdue);
console.log(`There are ${todosOverdue.length} overdue todos in the database.`);

}