/**
 * This file contains functions which interact with MongoDB, via mongoose, to perform Todo-related
 * CRUD operations.
 */
import { Todo } from './todos-schema';


// TODO Exercise Three: Implement the five functions below.

async function createTodo(todo) {
    const dbTodo = new Todo(todo);
    await dbTodo.save();
    return dbTodo;
}

async function retrieveAllTodos() {
    return await Todo.find();
}

async function retrieveTodo(id) {
    const todo = await Todo.findById(id);

    return todo ? todo : null;
}

async function updateTodo(todo) {
    const result = await Todo.findByIdAndUpdate(
        todo._id,
        todo,
        { new: true, useFindAndModify: false });

    return result ? true : false;
}

async function deleteTodo(id) {
    const result = await Todo.deleteOne({ _id: id });
    return result;
}

export {
    createTodo,
    retrieveAllTodos,
    retrieveTodo,
    updateTodo,
    deleteTodo
}