import React from 'react';
import useGet from './hooks/useGet';

const AppContext = React.createContext({
    todos: []
});

function AppContextProvider({ children }) {

    // TODO Exercise
    const {
        data: todos,
        isLoading: todosLoading,
        reFetch,
        update: updateTodo,
        deleteItem: deleteTodo,
        create: addTodo
    } = useGet('/api/todos', []);


    // The context value that will be supplied to any descendants of this component.
    const context = {
        todos,
        todosLoading,
        reFetch,
        updateTodo,
        deleteTodo,
        addTodo
    };

    // Wraps the given child components in a Provider for the above context.
    return (
        <AppContext.Provider value={context}>
            {children}
        </AppContext.Provider>
    );
}

export {
    AppContext,
    AppContextProvider
};