import { Switch, Route, Redirect, Link, useLocation } from 'react-router-dom';
import { AppContext } from './AppContextProvider';
import { useContext } from 'react';
import TablePage from './pages/TablePage';
import NewTodoPage from './pages/NewTodoPage';
import LoadingPage from './pages/LoadingPage';
import { AppBar, makeStyles, Tab, Tabs, Toolbar, Typography, CssBaseline, Fab } from '@material-ui/core';
import Footer from './components/Footer';
import dayjs from 'dayjs';
import TodoSummary from './components/TodoSummary';
import AddIcon from '@material-ui/icons/Add';
import { useHistory } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    backgroundColor: theme.palette.background.paper
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  title: {
    marginRight: theme.spacing(3)
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing(4),
    right: theme.spacing(4)
  }
}));

const navbarTabs = [
  { title: 'Todos', path: '/todos' },
  { title: 'Compose', path: '/newTodo' }
];

function useTabIndex() {
  const { pathname } = useLocation();
  for (let i = 0; i < navbarTabs.length; i++) {
    if (pathname.startsWith(navbarTabs[i].path))
      return i;
  }
  return 0;
}

function App() {

  const { todosLoading } = useContext(AppContext);
  const classes = useStyles();
  const tabIndex = useTabIndex();      
  const history = useHistory();


  return (

    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar variant="dense">

          <Typography variant="h6" className={classes.title}>
            MyTodo
          </Typography>

          <Tabs value={tabIndex} aria-label="Main navigation tabs">
            {navbarTabs.map((tab, index) => (
              <Tab key={index} label={tab.title} component={Link} to={tab.path} />
            ))}
          </Tabs>
          <TodoSummary />
        </Toolbar>
      </AppBar>

      <Toolbar variant="dense" />

      {todosLoading ? (
        <LoadingPage title="Loading todos..." />
      ) : (
        <Switch>
          <Route path="/todos">
            <TablePage />
            <Fab color="primary" className={classes.fab}
            onClick={() => history.push(`/newTodo`)}>
              <AddIcon />
            </Fab>
          </Route>
          <Route path="/newTodo">
            <NewTodoPage />
          </Route>
          <Route path="*">
            <Redirect to="/todos" />
          </Route>
        </Switch>
      )}

      <Footer title="MyTodo" description={`All your todos needs since ${dayjs().get('year')}`} />

    </div>
  );
}

export default App;