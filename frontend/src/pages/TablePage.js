import { React, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import relativeTime from 'dayjs/plugin/relativeTime';
import updateLocale from 'dayjs/plugin/updateLocale';
import { AppContext } from '../AppContextProvider';
import ExpandableTableRow from '../components/ExpandableTableRow';
import dayjs from 'dayjs';



dayjs.extend(relativeTime);
dayjs.extend(updateLocale);

dayjs.updateLocale('en', {
    relativeTime: {
        future: "in %s",
        past: "%s ago",
        s: 'a few seconds',
        m: "a minute",
        mm: "%d minutes",
        h: "an hour",
        hh: "%d hours",
        d: "a day",
        dd: "%d days",
        M: "a month",
        MM: "%d months",
        y: "a year",
        yy: "%d years"
    }
})

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650
    },
    tableFixHeader: {
        overflowY: 'auto',
    },
    theadTh: {
        position: 'sticky',
        top: 0,
        zIndex: theme.zIndex.drawer,
        background: 'white'
    }
}));


export default function TablePage() {
    const { todos } = useContext(AppContext);
    const classes = useStyles();

    const completedTodo = {
        _id: '605cf065e2f8ff1070e22503',
        title: 'Complete Todo Title',
        description: 'Complete Todo Desc',
        isCompleted: true,
        dueDate: dayjs().add(2, 'day').format()
    }

    return (
        <TableContainer component={Paper} className={classes.tableFixHeader}>
            <Table className={classes.table}
                aria-label="Todo table">
                <TableHead>
                    <TableRow>
                        <TableCell padding="checkbox" />
                        <TableCell className={classes.theadTh}>
                            <strong>Completed</strong>
                        </TableCell>
                        <TableCell className={classes.theadTh}>
                            <strong>ID</strong>
                        </TableCell>
                        <TableCell className={classes.theadTh}>
                            <strong>Title</strong>
                        </TableCell>
                        <TableCell className={classes.theadTh}>
                            <strong>Created Date</strong>
                        </TableCell>
                        <TableCell className={classes.theadTh}>
                            <strong>Due Date</strong>
                        </TableCell>
                        <TableCell className={classes.theadTh}>
                            <strong>Status</strong>
                        </TableCell>
                        <TableCell className={classes.theadTh}>
                            <strong>Delete</strong>
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {todos.map(todo => (
                        <ExpandableTableRow
                            key={todo._id}
                            todo={todo} />
                    ))}
                    <ExpandableTableRow
                            key={completedTodo._id}
                            todo={completedTodo} />
                </TableBody>
            </Table>
        </TableContainer>
    );
}
