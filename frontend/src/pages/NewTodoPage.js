import { useState, useContext } from 'react';
import Main from '../components/Main';
import { Typography, TextField, Grid, Button } from '@material-ui/core';
import { AppContext } from '../AppContextProvider';
import { useHistory } from 'react-router-dom';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { DateTimePicker } from '@material-ui/pickers';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import updateLocale from 'dayjs/plugin/updateLocale';
import Box from '@material-ui/core/Box';


dayjs.extend(relativeTime);
dayjs.extend(updateLocale);



export default function NewTodoPage() {

    const { addTodo } = useContext(AppContext);

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [hasErrors, setHasErrors] = useState(false);

    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    const [dueDate, setDueDate] = useState(tomorrow);

    const history = useHistory();


    const isError = (condition) => hasErrors && condition;

    async function handleAdd() {
        setHasErrors(true);

        if (title.length !== 0
            && title
            && dueDate) {
            const result = addTodo(
                {
                    title: title.trim(),
                    description: description.trim(),
                    dueDate,
                    isCompleted: false
                }
            );

            if (result) {
                history.push(`/todos`);
            }
        }
    }

    function handleCancel() {
        history.goBack();
    }

    return (
        <Box m={2} pt={3}>
            <Main title="Create your todo item!">
                <Grid container spacing={3} justify="space-between">
                    <Grid item xs={12}>
                        <Typography variant="h6" component="h6">Enter a title: (Compulsary)</Typography>
                        <TextField
                            autoFocus
                            margin="normal"
                            id="new-todo-title"
                            label="Title"
                            type="text"
                            fullWidth
                            value={title}
                            onChange={e => setTitle(e.target.value)}
                            error={isError(title.length === 0)}
                            helperText={isError(title.length === 0) && "Please enter a title!"}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <Typography variant="h6" component="h6">Enter a description: (Optional)</Typography>
                        <TextField
                            autoFocus
                            margin="normal"
                            id="new-todo-description"
                            label="Description"
                            type="text"
                            fullWidth
                            value={description}
                            onChange={e => setDescription(e.target.value)}
                            helperText={isError(description.length === 0) && "Please enter a description!"} />
                    </Grid>

                    <Grid item xs={12}>
                        <Typography variant="h6" component="h6">Choose a due date: (Compulsary)</Typography>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DateTimePicker
                                id="new-todo-duedate"
                                format="yyyy-MM-dd HH:mm"
                                margin="normal"
                                label="Due date"
                                fullWidth
                                value={dueDate}
                                onChange={(date) => setDueDate(date)} />
                        </MuiPickersUtilsProvider>
                    </Grid>

                    <Grid item />

                    <Grid item>
                        <Grid container spacing={1}>
                            <Grid item>
                                <Button variant="contained"
                                    color="primary"
                                    onClick={handleAdd}
                                    aria-label='Add'>
                                    Add 😀
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button variant="contained"
                                    onClick={handleCancel}
                                    aria-label='Cancel'>
                                    Cancel 😥
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Main>
        </Box>
    );
}
