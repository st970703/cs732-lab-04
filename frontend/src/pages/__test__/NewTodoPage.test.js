
import { render, fireEvent, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import NewTodoPage from '../NewTodoPage';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import updateLocale from 'dayjs/plugin/updateLocale';
import userEvent from '@testing-library/user-event';


dayjs.extend(relativeTime);
dayjs.extend(updateLocale);

const newTodo = {
    _id: '705cf065e2f8ff1070e99605',
    title: 'New Title',
    description: 'New Desc',
    isCompleted: false,
    dueDate: dayjs().add(1, 'day').format()
}


it('testing library - NewTodoPage add', () => {
    render(
        <NewTodoPage />
    );

    const txtTitle = screen.getByLabelText('Title');
    userEvent.type(txtTitle, newTodo.title);

    const txtDescription = screen.getByLabelText('Description');
    userEvent.type(txtDescription, newTodo.description);

    const addButton = screen.getByLabelText('Add');
    expect(addButton).not.toBeNull();
})

it('testing library - NewTodoPage cancel', () => {
    render(
        <NewTodoPage />
    );

    const txtTitle = screen.getByLabelText('Title');
    userEvent.type(txtTitle, newTodo.title);

    const txtDescription = screen.getByLabelText('Description');
    userEvent.type(txtDescription, newTodo.description);


    const cancelButton = screen.getByLabelText('Cancel');
    expect(cancelButton).not.toBeNull();
})