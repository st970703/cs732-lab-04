import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import updateLocale from 'dayjs/plugin/updateLocale';

dayjs.extend(relativeTime);
dayjs.extend(updateLocale);


function getStatus(todo) {
    if (todo.isCompleted) {
        return 'Completed';
    } else {
        const today = dayjs();
        const dueDate = dayjs(todo.dueDate);
        const tomorrow = dayjs().add(1, 'day');
        if (dueDate.isBefore(today, 'day')) {
            return 'Overdue';
        } else if (dueDate.isSame(today, 'day')
            || dueDate.isSame(tomorrow, 'day')) {
            return 'Upcoming';
        } else {
            return 'Incomplete';
        }
    }
}

export { getStatus }