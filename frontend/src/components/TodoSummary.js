import { AppContext } from '../AppContextProvider';
import { makeStyles } from '@material-ui/core/styles';
import { useContext } from 'react';
import Typography from '@material-ui/core/Typography';
import { getStatus } from '../utils/utils';


const useStyles = makeStyles((theme) => ({
    title: {
        flexGrow: 1,
    },
}));

function countStatuses(todos) {
    let statuses = {};

    todos.forEach(
        function (todo) {
            typeof statuses[getStatus(todo)] === 'undefined' ?
                statuses[getStatus(todo)] = 1 : statuses[getStatus(todo)]++;
        });

    return statuses;
}

export default function TodoSummary() {

    const classes = useStyles();

    const { todos } = useContext(AppContext);
    const statuses = countStatuses(todos);

    return (
        <Typography className={classes.title} style={{ alignSelf: 'center', display: 'flex', justifyContent: 'flex-end' }}>
            Todo details: {
                JSON.stringify(statuses, null, 2)
                    .replace(/[{}]/g, '')
                    .replace(/"([^"]+)":/g, '$1:')
                    .replace(/\s+$/, '.')
            }
                &nbsp;<strong>Total: {todos.length}</strong>
        </Typography>
    );
}