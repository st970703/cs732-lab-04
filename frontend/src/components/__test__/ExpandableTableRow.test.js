import ExpandableTableRow from '../ExpandableTableRow';
import dayjs from 'dayjs';
import { render, fireEvent, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import Table from '@material-ui/core/Table';
import relativeTime from 'dayjs/plugin/relativeTime';
import updateLocale from 'dayjs/plugin/updateLocale';


dayjs.extend(relativeTime);
dayjs.extend(updateLocale);



const completedTodo = {
    _id: '605cf065e2f8ff1070e22503',
    title: 'Completed Todo Title',
    description: 'Completed Todo Desc',
    isCompleted: true,
    dueDate: dayjs().add(2, 'day').format()
}

const overdueTodo = {
    _id: '605cf065e2f8ff1070e22605',
    title: 'Overdue Todo Title',
    description: 'Overdue Todo Desc',
    isCompleted: false,
    dueDate: dayjs().subtract(2, 'day').format()
}

const upcomingTodo = {
    _id: '705cf065e2f8ff1070e99605',
    title: 'Upcoming Todo Title',
    description: 'Upcoming Todo Desc',
    isCompleted: false,
    dueDate: dayjs().add(1, 'day').format()
}

const incompleteTodo = {
    _id: '705cf065e2f8ff1070e99605',
    title: 'Incomplete Todo Title',
    description: 'Incomplete Todo Desc',
    isCompleted: false,
    dueDate: dayjs().add(3, 'day').format()
}



it('testing library - completed todo', () => {  
    render(
            <TableContainer>
                <Table>
                    <TableHead>
                    </TableHead>
                    <TableBody>
                            <ExpandableTableRow
                                key={completedTodo._id}
                                todo={completedTodo} />
                    </TableBody>
                </Table>
            </TableContainer>
    );

    expect(screen.queryByText(completedTodo._id)).not.toBeNull();
    expect(screen.queryByText(completedTodo.title)).not.toBeNull();
    expect(screen.queryByText('Created a few seconds ago')).not.toBeNull();
    expect(screen.queryByText('Completed')).not.toBeNull();
    
    const checkbox = screen.getByLabelText('Complete checkbox');
    expect(checkbox).not.toBeNull();

    const deleteBtn = screen.getByLabelText('Delete');
    expect(deleteBtn).not.toBeNull();

    const toggleBtn = screen.getByLabelText('Toggle collapsible row');
    expect(toggleBtn).not.toBeNull();

    fireEvent.click(toggleBtn);
    expect(screen.queryByText(completedTodo.description)).not.toBeNull();
});

it('testing library - overdue todo', () => {  
    render(
            <TableContainer>
                <Table>
                    <TableHead>
                    </TableHead>
                    <TableBody>
                            <ExpandableTableRow
                                key={overdueTodo._id}
                                todo={overdueTodo} />
                    </TableBody>
                </Table>
            </TableContainer>
    );

    expect(screen.queryByText(overdueTodo._id)).not.toBeNull();
    expect(screen.queryByText(overdueTodo.title)).not.toBeNull();
    expect(screen.queryByText('Created a few seconds ago')).not.toBeNull();
    expect(screen.queryByText('Overdue')).not.toBeNull();
    
    const checkbox = screen.getByLabelText('Complete checkbox');
    expect(checkbox).not.toBeNull();

    const deleteBtn = screen.getByLabelText('Delete');
    expect(deleteBtn).not.toBeNull();

    const toggleBtn = screen.getByLabelText('Toggle collapsible row');
    expect(toggleBtn).not.toBeNull();

    fireEvent.click(toggleBtn);
    expect(screen.queryByText(overdueTodo.description)).not.toBeNull();
});

it('testing library - upcoming todo', () => {  
    render(
            <TableContainer>
                <Table>
                    <TableHead>
                    </TableHead>
                    <TableBody>
                            <ExpandableTableRow
                                key={upcomingTodo._id}
                                todo={upcomingTodo} />
                    </TableBody>
                </Table>
            </TableContainer>
    );

    expect(screen.queryByText(upcomingTodo._id)).not.toBeNull();
    expect(screen.queryByText(upcomingTodo.title)).not.toBeNull();
    expect(screen.queryByText('Created a few seconds ago')).not.toBeNull();
    expect(screen.queryByText('Upcoming')).not.toBeNull();
    
    const checkbox = screen.getByLabelText('Complete checkbox');
    expect(checkbox).not.toBeNull();

    const deleteBtn = screen.getByLabelText('Delete');
    expect(deleteBtn).not.toBeNull();

    const toggleBtn = screen.getByLabelText('Toggle collapsible row');
    expect(toggleBtn).not.toBeNull();

    fireEvent.click(toggleBtn);
    expect(screen.queryByText(upcomingTodo.description)).not.toBeNull();
});

it('testing library - incomplete todo', () => {  
    render(
            <TableContainer>
                <Table>
                    <TableHead>
                    </TableHead>
                    <TableBody>
                            <ExpandableTableRow
                                key={incompleteTodo._id}
                                todo={incompleteTodo} />
                    </TableBody>
                </Table>
            </TableContainer>
    );

    expect(screen.queryByText(incompleteTodo._id)).not.toBeNull();
    expect(screen.queryByText(incompleteTodo.title)).not.toBeNull();
    expect(screen.queryByText('Created a few seconds ago')).not.toBeNull();
    expect(screen.queryByText('Incomplete')).not.toBeNull();
    
    const checkbox = screen.getByLabelText('Complete checkbox');
    expect(checkbox).not.toBeNull();

    const deleteBtn = screen.getByLabelText('Delete');
    expect(deleteBtn).not.toBeNull();

    const toggleBtn = screen.getByLabelText('Toggle collapsible row');
    expect(toggleBtn).not.toBeNull();

    fireEvent.click(toggleBtn);
    expect(screen.queryByText(incompleteTodo.description)).not.toBeNull();
});