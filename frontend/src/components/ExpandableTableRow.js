import { useState, React, useContext } from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import dayjs from 'dayjs';
import Checkbox from '@material-ui/core/Checkbox';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import relativeTime from 'dayjs/plugin/relativeTime';
import updateLocale from 'dayjs/plugin/updateLocale';
import Typography from '@material-ui/core/Typography';
import Collapse from '@material-ui/core/Collapse';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core';
import { AppContext } from '../AppContextProvider';
import { getStatus } from '../utils/utils';
import Alert from '@material-ui/lab/Alert';



dayjs.extend(relativeTime);
dayjs.extend(updateLocale);

const useStyles = makeStyles((theme) => ({
    collapsibleCell: {
        paddingLeft: 55,
        paddingBottom: 0,
        paddingTop: 0
    }
}));

export default function ExpandableTableRow({ todo }) {
    const classes = useStyles();
    const [isExpanded, setIsExpanded] = useState(false);
    const { updateTodo, deleteTodo } = useContext(AppContext);
    const status = getStatus(todo);

    const alertMapping = {
    'Overdue': "error",
    'Upcoming': "warning",
    'Incomplete': "info"
}

    function handleDelete(event) {
        deleteTodo(todo._id);
        event.stopPropagation();
    }

    function handleComplete(event) {
        const newTodo = { ...todo };
        newTodo.isCompleted = event.target.checked;

        updateTodo(newTodo);
        event.stopPropagation();
    };


    return (
        <>
            <TableRow>
                <TableCell padding="checkbox">
                    <IconButton onClick={() => setIsExpanded(!isExpanded)}
                    aria-label='Toggle collapsible row'>
                        {isExpanded ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    <Checkbox
                        checked={todo.isCompleted}
                        color="primary"
                        id={todo._id}
                        inputProps={{ 'aria-label': 'Complete checkbox' }}
                        onChange={(e) => handleComplete(e)} />
                </TableCell>
                <TableCell>{todo._id}</TableCell>
                <TableCell>
                    {todo.title}
                </TableCell>
                <TableCell>
                    Created {dayjs(todo.createdAt).fromNow()}
                </TableCell>
                <TableCell>
                    {!todo.isCompleted ?
                        <>Due </> : <>Was due </>
                    }
                    <>{dayjs(todo.dueDate).fromNow()}</>
                </TableCell>
                <TableCell>
                {alertMapping[status] ?
                    <Alert severity={alertMapping[status]}>{status}</Alert>
                    : <>{status}</>
                }
                </TableCell>
                <TableCell>
                    <IconButton aria-label="Delete" onClick={(e) => handleDelete(e)}>
                        <DeleteForeverIcon color="secondary" />
                    </IconButton>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell className={classes.collapsibleCell} colSpan={8}>
                    <Collapse in={isExpanded} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Typography variant="h6" gutterBottom component="div">
                                Description
                            </Typography>
                            {todo.description}
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
}

