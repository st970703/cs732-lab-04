# CS732 Lab 04
**Note:** When running `npm install` to install the dependencies, run it with the additional tag `--legacy-peer-deps` so that dependencies resolve correctly:

```sh
npm install --legacy-peer-deps
```

In this lab, we'll be building a simple application almost from scratch, both frontend and backend - that is, full-stack development! The app we'll be building - and *testing* - is a simple todo-list app.

## Exercise One - Defining a schema
To begin, let's define the mongoose schema for a todo. Do this in the backend, [src/db/todos-schema.js](./backend/src/db/todos-schema.js).

Todos should have:

- An id (compulsory, unique)
- A title (compulsory)
- A description (optional)
- A created date (compulsory)
- A due date / time (compulsory)
- A completed status (compulsory)

**Hint:** Remember that, depending on the `mongoose.Schema` configuration used, some of the fields above will automatically be included without needing to define them.


## Exercise Two - Populating the database


## Exercise Three - Implementing a DAO


## Exercise Four - Implementing a REST service


## Exercise Five - Testing the REST service


## Exercise Six - Getting todos from the frontend


## Exercise Seven - The app's UI


## Exercise Eight - Testing the UI


## Exercise Nine - Adding and removing todos


## Exercise Ten - Updating the tests
